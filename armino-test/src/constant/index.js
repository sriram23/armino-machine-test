// This file holds all the mock data
export default {
    DISHDATASET: [
        {
            id: 1,
            image: 'https://www.thespruceeats.com/thmb/LDrkRByRnQInfZF25HyLYSJ0Iyg=/960x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/garlic-burger-patties-333503-hero-01-e4df660ff27b4e5194fdff6d703a4f83.jpg',
            dish: 'Super Dinner',
            restaurant: 'KFC Salmiya',
            rate: 2.35,
            hasPromocode: false,
        },
        {
            id: 2,
            image: 'https://www.thespruceeats.com/thmb/LDrkRByRnQInfZF25HyLYSJ0Iyg=/960x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/garlic-burger-patties-333503-hero-01-e4df660ff27b4e5194fdff6d703a4f83.jpg',
            dish: 'Super Dinner',
            restaurant: 'KFC Salmiya',
            rate: 2.35,
            hasPromocode: true,
        },
        {
            id: 3,
            image: 'https://www.thespruceeats.com/thmb/LDrkRByRnQInfZF25HyLYSJ0Iyg=/960x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/garlic-burger-patties-333503-hero-01-e4df660ff27b4e5194fdff6d703a4f83.jpg',
            dish: 'Super Dinner',
            restaurant: 'KFC Salmiya',
            rate: 2.35,
            hasPromocode: true,
        }
    ],
    CHECKOUTDATASET: {
        subTotal: 10.35,
        discount: 2.0,
    },
    CAROUSELDATA: [
        {
            id: 1,
            image: 'https://images-na.ssl-images-amazon.com/images/I/51YokupX1ML._SL1000_.jpg',
            dish: 'Nivea body milk 48h Nivea body Milk 48h (1)',
            rate: 1.200,
            discount: 20,
            time: '10 mins',
        },
        {
            id: 2,
            image: 'https://images-na.ssl-images-amazon.com/images/I/51YokupX1ML._SL1000_.jpg',
            dish: 'Nivea body milk 48h Nivea body Milk 48h (2)',
            rate: 1.200,
            discount: 20,
            time: '10 mins',
        },
        {
            id: 3,
            image: 'https://images-na.ssl-images-amazon.com/images/I/51YokupX1ML._SL1000_.jpg',
            dish: 'Nivea body milk 48h Nivea body Milk 48h (3)',
            rate: 1.200,
            discount: 20,
            time: '10 mins',
        },
        {
            id: 4,
            image: 'https://images-na.ssl-images-amazon.com/images/I/51YokupX1ML._SL1000_.jpg',
            dish: 'Nivea body milk 48h Nivea body Milk 48h (4)',
            rate: 1.200,
            discount: 20,
            time: '10 mins',
        },
        {
            id: 5,
            image: 'https://images-na.ssl-images-amazon.com/images/I/51YokupX1ML._SL1000_.jpg',
            dish: 'Nivea body milk 48h Nivea body Milk 48h (5)',
            rate: 1.200,
            discount: 20,
            time: '10 mins',
        },
        {
            id: 6,
            image: 'https://images-na.ssl-images-amazon.com/images/I/51YokupX1ML._SL1000_.jpg',
            dish: 'Nivea body milk 48h Nivea body Milk 48h (6)',
            rate: 1.200,
            discount: 20,
            time: '10 mins',
        },
    ],
}