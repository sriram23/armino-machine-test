import Vue from 'vue'
import VueRouter from 'vue-router';

import ArminoHome from '../components/pages/home.vue'

Vue.use(VueRouter)
export default new VueRouter(
    {
        routes: [
            {
                path: '/',
                name: 'Armino Home',
                component: ArminoHome,  
            }
        ]
    }
)